Holds random scripts/stuffing around with Junethack 2014 data.

Compile these from RMD with a combination of knitr and pandoc:

```r
# from R
library(knitr)

# 1. rmd to md
tmp <- tempfile(fileext=".md")
knit('filename.rmd', 'filename.md')

# 2. md to html. use the pandoc function in R, or
#    (my preferred) pandoc command-line
# e.g. pandoc filename.md -t html --standalone --self-contained --table-of-contents
pandoc('filename.md', 'html')
```
