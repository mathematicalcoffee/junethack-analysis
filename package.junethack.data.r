# Script to scrape the Junethack website and download the users/clans etc
# Also loads ~/logfiles/xlogfiles.rda which has all the xlogfiles for all the
# Junethack servers (I'll put up that script eventually too)
#
# And really just adds enough info to the xlogfiles that you can look up
#  particular users on junethack on various servers (e.g. the 'coffeebug'
#  account has a player 'coffeebug' on NAO and 'coffeebeet' on dnethack
#  server).
#
# Also saves clan information.
#
# Warning: script not really checked, it worked for me. It downloads lots of
#  files and expects that xlogfiles.rda already exists.
verbose <- T
if (!interactive() | !verbose) message <- capture.output
# ------------ 1. load junethack participants -------------- #
library(data.table)
library(XML)

clan.url <- 'https://junethack.de/clan/'
user.url <- 'https://junethack.de/user/'
clans.url <- 'https://junethack.de/clans'
users.url <- 'https://junethack.de/users'
dat.dir <- 'html'

get.table <- function (url, out, which, force=F) {
    if (!file.exists(out) || force) {
        download.file(url, destfile=out, quiet=T, method='curl')
    }
    doc <- htmlParse(out)
    tbl <- tryCatch({
        tbl <- readHTMLTable(doc, trim=T, which=which, stringsAsFactors=F)
        names(tbl) <- gsub('^\\s+|\\s+$', '', names(tbl))
        tbl
    }, error=function (e) NULL)
    tbl
}

message("getting list of users...")
# 1. get list of users
users.html <- file.path(dat.dir, 'users.html')
users <- data.table(get.table(users.url, users.html, 1, force=F))
setnames(users, c('Games Played', 'Trophies Earned'), c('Games', 'Trophies'))
users[, Accounts:=as.numeric(Accounts)]
users[, Games:=as.numeric(Games)]
users[, Trophies:=as.numeric(Trophies)]

# account-level
users[, Played:=Games]
users[, Scummed:=0]

message("looking up each user...")
library(stringr)
# 2. look up the accounts per user
# TODO CASE SENSITIVITIES ON WINDOWS
tmp <- make.unique(tolower(users$Username))
tmp[!duplicated(tolower(users$Username))] <- NA
tmp <- rbindlist(lapply(seq_along(users$Username),
              function (i) {
                  us <- users$Username[i]
                  # to deal with not-case-sensitive filesystems (stth vs SttH)
                  fname <- file.path(dat.dir,
                                     paste0('user.', ifelse(is.na(tmp[i]), us, tmp[i]), '.html'))
                  t <- get.table(paste0(user.url, us), fname, which=1)

                  if (is.null(t)) t <- list()

                  # store games startscummed (played==from table)
                  doc <- htmlParse(fname)
                  h2s <- unique(xmlSApply(getNodeSet(doc, "//h2"), xmlValue, trim=T))
                  if (length(h2s)) {
                      tt <- na.omit(str_match(h2s, '([0-9]+) Games? Start Scummed')[, 2])
                      if (length(tt)) users[i, Scummed:=as.numeric(tt)]
                  }

                  # return table snippet
                  t$Username <- us
                  t
              }), fill=T)

# 3. Merge tmp into users
setkey(users, Username)
setkey(tmp, Username)
users <- users[tmp]
rm(tmp)

users[, Games:=Played+Scummed]

# split server and variant
users[, Variant:=factor(str_match(Server, '\\((.*?)\\)')[, 2])]
users[, Server:=factor(gsub(' \\(.*\\)', '', Server))]

# 4. look up clans.
clans.html <- file.path(dat.dir, 'clans.html')
clans.tbl <- get.table(clans.url, clans.html, 1, force=F)

message("getting clans ...")
# 5. download info of each clan
clans <- rbindlist(lapply(clans.tbl[[1]],
                          function (cl) {
                              t <- get.table(paste0(clan.url, cl),
                                             file.path(dat.dir, paste0('clan.', cl, '.html')),
                                             which=1)
                              t$Clan <- cl
                              t
                          }), fill=T)

# join clan information into users
setnames(clans, 'Player', 'Username')
setkey(clans, Username)
users[clans, Clan:=Clan]

# VERIFY
x <- all.equal(users[!is.na(Clan), list(N=length(unique(Username))), by=Clan][order(Clan)],
               clans[, .N, by=Clan][order(Clan)])
if (!isTRUE(x)) {
    message("inequality between users and clans table: ", x)
}

# SPLIT into two tables: junethack.users and users
# junethack.users has junethack-account-wide stats (games played, scummed, trphies)
# users has per-server information (server, account, username, name)
setnames(users, 'Username', 'Junethack.Username')
junethack.users <- users[, list(Junethack.Username, Accounts, Games, Trophies, Played, Scummed, Clan)]
users <- users[, list(Junethack.Username, Account, Server, Variant, Clan)]

setkeyv(junethack.users, names(junethack.users))
junethack.users <- unique(junethack.users)

message("saving users and clans ... ")
# SAVE
save(users, junethack.users, clans, file='junethack.rda')

# ---------------- 2. load xlogfile ----------------- #

# The Junethack version
normalize.deaths <- function(deaths) {
    # sic. the 'killey' is a typo on that site.
    deaths <- gsub('^killey by an ', 'killed by a ', deaths)
    deaths <- gsub(', while .*', '', deaths)

    deaths <- gsub('hallucinogen-distorted ', '', deaths)
    
    deaths <- gsub('by (the )?invisible ', 'by ', deaths)
    deaths <- gsub('by (an|a) invisible ', 'by a ', deaths)
    deaths <- gsub('by invisible ', 'by a ', deaths)
    
    deaths <- gsub(' (her|his) ', ' eir ', deaths)
    deaths <- gsub(' (her|him)self ', ' eirself ', deaths)
    deaths <- gsub(' (her|him)self$', ' eirself', deaths)
    
    deaths <- gsub(' (called|named) .*', '', deaths)
    
    deaths <- gsub(' \\(with the Amulet\\)$', '', deaths)
    
    deaths <- gsub('choked on .*', 'choked on something', deaths)
    
    deaths <- gsub('killed by kicking .*', 'killed by kicking something', deaths)
    
    # deaths <- gsub('^killed by (the|an?) ', 'killed by ', deaths)
    return(deaths)
}

# My version
normalize.deaths.extra <- function (deaths) {
    # sic. the 'killey' is a typo on that site.
    deaths <- gsub('^killey by an ', 'killed by a ', deaths)

    # convert 'killed by a foo, while bar' to 'killed, while bar'
    #  (up to you to be killed by them while *not* bar)
    # NOTE: preserve first verb, is this nonsensical?
    # NOTE: ', while' occurs AFTER 'named', so do not die to a named animal
    #       or you will lose this.
    deaths <- gsub('^([^ ]+).*, while (.*)$', '\\1, while \\2', deaths)

    # 'hallucinogen-distorted'
    deaths <- gsub('hallucinogen-distorted .*', 'hallucinogen-distorted monster', deaths)

    # 'invisible'
    deaths <- gsub('by (the )?invisible ', 'by ', deaths)
    deaths <- gsub('by (an|a) invisible ', 'by a ', deaths)
    deaths <- gsub('by invisible ', 'by a ', deaths)
    deaths <- gsub('by a invisible (hallucinogen-distorted )?.*', 'by an invisible \\1monster', deaths)
    
    deaths <- gsub(' (her|his) ', ' eir ', deaths)
    deaths <- gsub(' (her|him)self ', ' eirself ', deaths)
    deaths <- gsub(' (her|him)self$', ' eirself', deaths)
    
    # note: named strips everything after it, so don't try to get ', while bar'
    #  conditions with a naemd monster.
    deaths <- gsub(' (called|named) .*', '', deaths)

    deaths <- gsub(' \\(with the Amulet\\)$', '', deaths)
    
    deaths <- gsub('choked on .*', 'choked on something', deaths)
    
    deaths <- gsub('killed by kicking .*', 'killed by kicking something', deaths)
   
    # killed by a falling {foo}
    deaths <- gsub("^killed by a falling (?!rock).*$", "killed by a falling object", deaths, perl=T)

    # strip ghost names
    deaths <- gsub(" ghost of .*", " ghost", deaths)

    # food poisoning
    deaths <- gsub("^poisoned by a rotted .* corpse", "poisoned by a rotted corpse", deaths)

    # dieties
    deaths <- gsub(" wrath of .*", " wrath of a diety", deaths)

    # priests and priestesses
    deaths <- gsub("priest(ess)?", "priest(ess)", deaths)

    # shopkeepers
    deaths <- gsub("M[rs]\\. [A-Z].*, the shopkeeper", "a shopkeeper", deaths)

    # a {monster}'s {item}: tricky because of general regex looking for `'`
    # e.g. Durin's Bane, gas spore's explosion, quit while already on Charon's boat
    # while frozen by a monster's gaze
    # deaths <- gsub("^(\\w+) by (an?|the) .*'s (?!explosion.*)$", "\\1 by a monster's \\2", deaths, perl=T)
    # narrow it down to wands only?


    # ?? touching (an artifact)?
    deaths <- gsub("killed by touching .*", "killed by touching an artifact", deaths)
    
    # ?? minions of dieties? "minion of a diety"? "minion of {diety}"? "{minion} of a deity"?
    # the first: 1 death. the second: 39 deaths (#gods). third: #minions deaths.
    deaths <- gsub("(\\w+ elemental|Aleax|couatl|Angel|\\w+ demon|\\w+ devil|(suc|in)cubus|balrog|pit fiend|nalfeshnee|hezrou|vrock|marilitherinyes) of [A-Z].*", "minion of a diety", deaths)

    # ?? racial priest*s?

    # return
    deaths
}

message("loading xlogfiles ... ")
# load all logfiles
load('~/logfiles/all.xlogfiles.rda')
logfile.varnames = grep('xlogfile$', ls(), value=T)

# extract just:
# * june logs
# * from participants
# mapping from users${Server, Variant} to the varname
xlogfile.mapping <- data.table(
    xlogfile=c('acehack.acehack.xlogfile',
               'acehack.nh1.3d.xlogfile',
               'acehack.vanilla.xlogfile',
               'dnethack.xlogfile',
               'grunthack.xlogfile',
               'nao.xlogfile',
               'nethack4.xlogfile',
               'sporkhack.xlogfile',
               'unnethack.xlogfile'),
    server=c('acehack.de',
             'acehack.de',
             'acehack.de',
             'dnethack.ilbelkyr.de',
             'grunthack.org',
             'nethack.alt.org',
             'nethack4.org',
             'sporkhack.com',
             'un.nethack.nu'),
    variant=c('acehack',
              'oldhack',
              'vanilla',
              'dnethack',
              'grunthack',
              'vanilla',
              'nethack4',
              'sporkhack',
              'unnethack')
)
setkey(xlogfile.mapping, server, variant)
setkey(users, Server, Variant)
message("filtering deaths ... ")
deaths <- rbindlist(lapply(logfile.varnames,
    function (vn) {
        var <- sub('.xlogfile', '', vn, fixed=T)
        tbl <- get(vn)
        # I convert to GMT, bah
        tbl[, starttime:=as.POSIXct(as.numeric(starttime), origin="1970-01-01", tz="GMT")]
        tbl[, endtime:=as.POSIXct(as.numeric(endtime), origin="1970-01-01", tz="GMT")]

        # filter out June only
        tbl <- tbl[year(starttime)==2014 & month(starttime) == 6 & month(endtime) == 6]

        # filter out junethack players only
        tbl[, server:=xlogfile.mapping[xlogfile==vn, server]]
        tbl[, variant:=xlogfile.mapping[xlogfile==vn, variant]]
        setkey(tbl, name)
        tbl <- tbl[J(users[xlogfile.mapping[xlogfile==vn], Account]), nomatch=0]

        # remove "junk games" (mode==explore, multiplayer, debug, polyinit)
        # https://github.com/junethack/Junethack/blob/cb45a68ac2e57d255e43a90c23b2761cb21c77df/lib/junethack/fetch_games.rb#L68
        if ('mode' %in% names(tbl)) {
            tbl[is.na(mode) | !(mode %in% c('explore', 'multiplayer', 'debug', 'polyinit'))]
        } else {
            tbl
        }
    }),
    fill=T)
deaths[, variant:=factor(variant)]
deaths[, server:=factor(server)]

setkey(deaths,name,variant,server)
setkey(users,Account,Variant,Server)
# scummed: https://github.com/junethack/Junethack/blob/cb45a68ac2e57d255e43a90c23b2761cb21c77df/lib/junethack/fetch_games.rb#L64
# should be escaped/quit and turns <= 10

invisible(capture.output(suppressMessages({
# Might not be equal:
# 1. if a junk game is also startscummed, it's counted as startscummed by junethack.de
#    but I discard it as a junk game (kerio)
# 2. For some reason I found one startscummed game that was not counted
#    by junethack.de (Frogging101)
x <- all.equal(
          (u.1 <- junethack.users[, list(N=sum(Games)), by=Junethack.Username][N>0][order(Junethack.Username)])
          ,
          (u.2 <- deaths[users, nomatch=0][, .N, by=Junethack.Username][N>0][order(Junethack.Username)])
)
if (!isTRUE(x)) {
    message("Discrepancy in deaths lookup: ", x)
    idx <- which(u.1$N - u.2$N != 0)
    print(u.1[idx])
    print(u.2[idx])
}
})))

deaths[, unique.death.junethack:=normalize.deaths(death)]
deaths[, unique.death.me:=normalize.deaths.extra(death)]

## record startscumming
deaths[, startscummed:=death %in% c('escaped', 'quit') & turns <= 10]
## record clan
setkey(users, Account, Server, Variant)
setkey(deaths, name, server, variant)
deaths[users, nomatch=0, clan:=Clan]

message("saving deaths ... ")
save(deaths, users, junethack.users, clans, file='junethack.2014.rda')

## SAVE AS CSV and ZIP
if (file.exists('junethack.2014.zip')) unlink('junethack.2014.zip')
d <- tempdir()
write.table(users, file=file.path(d, "users.csv"), quote=T, sep=',', row.names=F, qmethod='escape')
write.table(junethack.users, file=file.path(d, "junethack.users.csv"), quote=T, sep=',', row.names=F, qmethod='escape')
write.table(clans, file=file.path(d, "clans.csv"), quote=T, sep=',', row.names=F, qmethod='escape')
# convert starttime/endtime back to seconds since unix epoch
write.table(transform(deaths, starttime=as.integer(starttime), endtime=as.integer(endtime)),
            file=file.path(d, "deaths.csv"), quote=T, sep=',', row.names=F, qmethod='escape')
zip('junethack.2014.zip', file.path(d, c('users.csv', 'junethack.users.csv', 'clans.csv', 'deaths.csv')), '-rj9X')

# TODO: get all the other years too (may as well).
message("done.")


